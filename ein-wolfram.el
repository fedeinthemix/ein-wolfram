;;; ein-wolfram.el --- Support for Wolfram Language in EIN. -*- lexical-binding: t; -*-

;; Copyright (C) 2019,2020 Federico Beffa

;; Author: F. Beffa <beffa@fbengineering.ch>
;; Version: 1.0
;; Package-Requires: ((wolfram-mode "20180307.13") (ein "20190601.1746") (emacs "24.3"))
;; Keywords: Wolfram Language, Jupyter, EIN
;; URL: https://gitlab.com/fedeinthemix/ein-wolfram

;; ein-wolfram is free software; you can redistribute it and/or modify it
;; under the terms of the GNU General Public License as published by
;; the Free Software Foundation; either version 3, or (at your option)
;; any later version.

;; ein-wolfram is distributed in the hope that it will be useful, but
;; WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
;; General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with AUCTeX; see the file COPYING.  If not, write to the Free
;; Software Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA
;; 02110-1301, USA.

;;; Commentary:

;; Defines a setup procedure for using the Jupyter kernel
;; 'WolframLanguageForJupyter'
;; (https://github.com/WolframResearch/WolframLanguageForJupyter) with
;; EIN.  It is strongly recommended to install the 'wolfram-mode'
;; Emacs package as this package makes use of feasures (such as
;; font-lock, ...) defined in that mode.

;; Installation: Put the following command in your .emacs file
;;
;; (with-eval-after-load 'ein
;;   (load "/path-to/ein-wolfram"))

;;; Code:

(require 'wolfram-mode "wolfram-mode" t)

;; EIN looks for a language setup procedure whose name is as follows
;;
;; (concat "ein:ml-lang-setup-" (ein:$kernelspec-language kernelspec))
;;
;; see 'ein:ml-lang-setup' in "ein-multilang.el".
;;
;; The Wolfram kernel language name defined by
;; https://github.com/WolframResearch/WolframLanguageForJupyter is
;; "Wolfram Language". Default configuration file location:
;;
;; ".local/share/jupyter/kernels/wolframlanguage12/kernel.json"
;;
;; This defined the name of the following procedure.

;;;###autoload
(defun ein:ml-lang-setup-Wolfram-Language ()
  "Based on `wolfram-mode'."
  (setq-local mode-name "EIN[WL]")
  (setq-local comment-start "(*")
  (setq-local comment-end "*)")
  (setq-local comment-start-skip "(\\*")
  (when (featurep 'wolfram-mode)
    (set-syntax-table wolfram-mode-syntax-table)
    (set (make-local-variable 'syntax-propertize-function)
         wolfram-syntax-propertize-function)
    (setq-local font-lock-defaults '(wolfram-font-lock-keywords nil nil))
    (setq-local outline-regexp wolfram-outline-regexp)
    (setq-local indent-line-function
                (apply-partially #'ein:ml-indent-line-function #'indent-line-function))
    (setq-local indent-region-function
                (apply-partially #'ein:ml-indent-region))
    (set-keymap-parent ein:notebook-multilang-mode-map nil)
    (smie-setup wolfram-smie-grammar #'wolfram-smie-rules
                :forward-token 'wolfram-smie-forward-token
                :backward-token 'wolfram-smie-backward-token)))

;; 'ein:ml-fontify-1' fontify based on the language name of the
;; cell. (See "ein-multilang.el")
;; (when (featurep 'wolfram-mode)
;;   (define-derived-mode Wolfram-Language-mode wolfram-mode "WL-mode"))

;;;###autoload
(define-derived-mode Wolfram-Language-mode wolfram-mode "WL-mode")

(provide 'ein-wolfram)

;;; ein-wolfram.el ends here
