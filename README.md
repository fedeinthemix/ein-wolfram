# Ein-Wolfram

Integration of the Jupyter kernel
[WolframLanguageForJupyter](https://github.com/WolframResearch/WolframLanguageForJupyter)
with [EIN](https://github.com/millejoh/emacs-ipython-notebook).

# Requirements

- [EIN](https://github.com/millejoh/emacs-ipython-notebook)
- [WolframLanguageForJupyter](https://github.com/WolframResearch/WolframLanguageForJupyter)
- [wolfram-mode](https://github.com/kawabata/wolfram-mode)

# Installation

Download the file and install it from within Emacs with `M-x
package-install-file`. 

Alternatively byte-compile the file and put the following in your
'.emacs' file
```
(with-eval-after-load 'ein
  (load "/path-to/ein-wolfram"))
```
